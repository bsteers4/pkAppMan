#!/usr/bin/env bash

# Gambas component Installer script for Gambas basic application 'pkAppMan2.0.gambas'

# Must be run from terminal, will give option to install full Gambas3 or minimum components

# Function to check for installed package installer
GetPKGR() {
echo "Checking for installed packager apt, yum or dnf..."
PKGR=$(which apt-get)
if [ "$PKGR" = "" ]; then
 PKGR=$(which yum)
 if [ "$PKGR" = "" ]; then
  PKGR=$(which dnf)
  if [ "$PKGR" = "" ]; then
  echo -n "Sorry cannot find your package installer\nPress return to exit."
  read
  exit
  fi
 fi
fi

echo "found packager '$PKGR', for install okay."
}

# Function to ask user what type of install they want and start installing...
SelectInstall() {
echo "Install $AppName's minimum required gambas components or the complete Gambas3 package?"
echo -ne "Press return for minimum components (default) or type 'f' for full Gambas3\nor Ctrl-C to cancel: "
read REPLY
if [ "${REPLY,,}" = "f" ]; then
 echo "Installing the complete Gambas3 developement package..."
 sudo "$PKGR" install gambas3
 echo "Gambas3 full install complete\nEnsuring application components are present.."
 sleep 0.5
 NeededComponents
else
NeededComponents
fi

echo "Gambas component install finished."
}

# function to check for missing required gambas components and make a list $DEPLIST
NeededComponents() {

if [ ! -e "/usr/lib/gambas3" ]; then
DEPLIST="$(echo "$DEPS"|tr '.' '-')"
DEPLIST="gambas3-$(echo "$DEPLIST"|sed 's/ / gambas3-/g')"
else
INSTALLED=$(ls -Q /usr/lib/gambas3/*.component)
TIFS=$IFS
IFS='\"'

read -d '\"' -a CO_ARRAY <<< $INSTALLED
IFS=' '
read -a DEP_ARRAY <<< $DEPS
CNT=0
DEPLIST=""

while [ $CNT -lt ${#DEP_ARRAY[@]} ]; do
IsInstalled "${DEP_ARRAY[$CNT]}"

if [ $IsInstalled -eq 0 ]; then
NDED="gambas3-"$(echo "${DEP_ARRAY[$CNT]}"|tr '.' '-')
 if [ "$DEPLIST" = "" ]; then
 DEPLIST="$NDED"
 else
 DEPLIST="$DEPLIST $NDED"
 fi
fi
((CNT++))
done
fi
AddRuntime

if [ "$DEPLIST" = "" ]; then
echo "No additional gambas3 components need to be installed.."
else
echo -e "Installing the following items..\n'$DEPLIST'"
echo -ne "Press return to continue or Ctrl-C to exit: "
read

 sudo "$PKGR" install -y $DEPLIST

fi
}

# Function. check for gbr3 in the path and add gambas3-runtime if not found
AddRuntime() {
 RT_Yes=$(which gbr3)
 if [ -z "$RT_Yes" ]; then DEPLIST="gambas3-runtime $DEPLIST"; fi
}

# Function Check if a component exists in the list
IsInstalled() {
if [ "${#CO_ARRAY[@]}" = "0" ]; then
IsInstalled=0
return
fi
ICNT=0
while [ $ICNT -lt ${#CO_ARRAY[@]} ]; do
CNAME="${CO_ARRAY[$ICNT]##*/}"
CNAME="${CNAME%.*}"
if [ "$CNAME" = "$1" ]; then
IsInstalled=1
return
fi
((ICNT++))
done
IsInstalled=0
}

GambasFiletype() {
# Check xdg components are installed..
if [ -e "/usr/share/mime/packages/application-x-gambas3.xml" ]; then
echo "Skipping Gambas3 application filetype install, already configured."
return
fi
XDGYes=$(which xdg-mime)
if [ ! -z "$XDGYes" ]; then
XDGYes=$(which xdg-icon-resource)
fi
if [ -z "$XDGYes" ]; then
echo -ne "xdg not installed, cannot setup gambas filetype."
return
fi

# Check application-x-gambas3.png icon exists in current folder and install it.
echo "Installing default filetype icon.."
if [ -e "./.public/application-x-gambas3.png" ]; then
 sudo xdg-icon-resource install --context mimetypes --size 48 ./.public/application-x-gambas3.png x-application-x-gambas3
else
 echo -ne "error, application-x-gambas3.png icon not found, cannot configure filetype."
 return
fi

echo -e "done.\n\nNow installing mime filetype and updating database..\nPlease wait a moment..."

# write temp default fyletype xml file from Gambas3 distro
echo -e '<?xml version="1.0" encoding="UTF-8"?>\n<mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
	<mime-type type="application/x-gambas3">\n		<sub-class-of type="application/x-executable"/>
		<comment>Gambas 3 executable</comment>\n		<comment xml:lang="fr">Exécutable Gambas 3</comment>
		<magic priority="50">\n			<match type="string" value="#! /usr/bin/env gbr3" offset="0"/>\n		</magic>
		<glob pattern="*.gambas" />\n	</mime-type>\n</mime-info>' >/tmp/application-x-gambas3.xml

# install filetype and remove temp file
sudo xdg-mime install /tmp/application-x-gambas3.xml
rm /tmp/application-x-gambas3.xml
echo "Now updating mime database..."
sudo update-mime-database /usr/share/mime
echo -ne "done.\napplcation-x-gambas3 filetype install completed."
}


AppName="pkAppMan2.0.gambas"
DEPS="gb.args gb.image gb.gui gb.form gb.stock gb.jit"


GetPKGR
SelectInstall
GambasFiletype

echo "Install script completed, press return to finish."
read

